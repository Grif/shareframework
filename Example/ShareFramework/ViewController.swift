//
//  ViewController.swift
//  ShareFramework
//
//  Created by Marius Avram on 01/11/2019.
//  Copyright (c) 2019 Marius Avram. All rights reserved.
//

import UIKit
import ShareFramework
import SwiftyDropbox

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, OneDriveControllerDelegate, GoogleDriveViewControllerDelegate {
    
    var ftp = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: TableView data source & delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 5
        case 2:
            return 3
        case 3:
            return 4
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "SOCIAL"
        case 1:
            return "UPLOAD / TRANSFER"
        case 2:
            return "iOS"
        case 3:
            return "3RD PARTY"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell")
        cell?.detailTextLabel?.text = ""
        
        switch indexPath.section {
        case 0:
            cell?.textLabel?.text = "Facebook"
        case 1:
            switch indexPath.row {
            case 0:
                cell?.textLabel?.text = "FTP"
            case 1:
                cell?.textLabel?.text = "SFTP"
            case 2:
                cell?.textLabel?.text = "Bluetooth"
            case 3:
                cell?.textLabel?.text = "Wifi"
            case 4:
                cell?.textLabel?.text = "HTTP Upload / Download"
            default:
                cell?.textLabel?.text = ""
            }
        case 2:
            switch indexPath.row {
            case 0:
                cell?.textLabel?.text = "AirDrop"
            case 1:
                cell?.textLabel?.text = "CloudKit"
                cell?.detailTextLabel?.text = "store / access data for current iCloud account"
            case 2:
                cell?.textLabel?.text = "iCloud Drive"
                cell?.detailTextLabel?.text = "upload/download files on iCloud drive"
            default:
                cell?.textLabel?.text = ""
            }
        case 3:
            switch indexPath.row {
            case 0:
                cell?.textLabel?.text = "Dropbox - Upload"
            case 1:
                cell?.textLabel?.text = "Dropbox - Download"
            case 2:
                cell?.textLabel?.text = "One Drive"
            case 3:
                cell?.textLabel?.text = "Google Drive"
            case 4:
                cell?.textLabel?.text = "Box.com"
            default:
                cell?.textLabel?.text = ""
            }
        default:
            break
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.setSelected(false, animated: true)
        }
        
        switch indexPath.section {
        case 0:
            ShareFacebook.sharedInstance.shareURL(URL(string: "https://www.google.ro")!, from:self)
        case 1:
            switch indexPath.row {
            case 0:
                ftp = true
                self.performSegue(withIdentifier: "ftpFromHome", sender: self)
            case 1:
                ftp = false
                self.performSegue(withIdentifier: "ftpFromHome", sender: self)
            case 2:
                self.performSegue(withIdentifier: "bluetoothFromHome", sender: self)
            case 3:
                self.performSegue(withIdentifier: "wifiFromHome", sender: self)
            case 4:
                self.performSegue(withIdentifier: "httpUploadDownload", sender: self)
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                if let path = Bundle.main.path(forResource: "sound", ofType: "aiff") {
                    iOSShare.sharedInstance.airDropShare(fromViewController: self.navigationController!, file: path)
                }
            case 1:
                self.performSegue(withIdentifier: "icloudPersistFromHome", sender: self)
            case 2:
                self.performSegue(withIdentifier: "icloudDriveFromMain", sender: self)
            default:
                break
            }
        case 3:
            switch indexPath.row {
            case 0:
                if DropboxClientsManager.authorizedClient == nil {
                    let alert = UIAlertController(
                        title: "Authenticate to Dropbox",
                        message: "You need to authenticate and allow the app to access your dropbox. Then repeat your action.",
                        preferredStyle: UIAlertController.Style.alert
                    )
                    let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
                        DropboxController.sharedInstance.authenticateToDropbox(controller: self)
                    }
                    alert.addAction(ok)
                    present(alert, animated: true, completion: nil)
                }
                else if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
                    do {
                        let data = try Data(contentsOf: URL(fileURLWithPath: path))
                        DropboxController.sharedInstance.upload(folder: "shared_framework", fileName: "mxkid.jpg", fileData: data) { (success, error) in
                            if success {
                                
                            }
                        }
                    } catch {
                        self.showAlert(title: "Error", message: "Failed to read file")
                    }
                }
            case 1:
                if DropboxClientsManager.authorizedClient == nil {
                    let alert = UIAlertController(
                        title: "Authenticate to Dropbox",
                        message: "You need to authenticate and allow the app to access your dropbox. Then repeat your action.",
                        preferredStyle: UIAlertController.Style.alert
                    )
                    let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
                        DropboxController.sharedInstance.authenticateToDropbox(controller: self)
                    }
                    alert.addAction(ok)
                    present(alert, animated: true, completion: nil)
                }
                else  {
                    let viewController = DropboxController.sharedInstance.showDownload(parentController: self)
                    viewController.delegate = self
                }
            case 2:
                if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
                    OneDriveController.sharedInstance.delegate = self
                    OneDriveController.sharedInstance.upload(accountAppId: "6ddbf115-d906-4f2d-bfc8-705799274f65", filePath: path, fileName: "mxkid.jpg")
                }
            case 3:
                if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
                    GoogleDriveViewController.sharedInstance.initialize(clientId: "387536636889-oei66qgtpnkpcdehafhv81sthoelk8gm.apps.googleusercontent.com",
                                                                        redirectURI:"com.googleusercontent.apps.387536636889-oei66qgtpnkpcdehafhv81sthoelk8gm:/oauthredirect")
                    //GoogleDriveViewController.sharedInstance.upload(viewController: self, fileName: "mxkid.jpg", filePath: path)
                    GoogleDriveViewController.sharedInstance.list(viewController: self)
                    GoogleDriveViewController.sharedInstance.delegate = self
                }
            default:
                break
            }
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ftpFromHome" {
            (segue.destination as! FTPViewController).ftp = ftp
        }
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    //MARK: OneDrive delegate
    func uploadFailed(error: String) {
        showAlert(title: "Failed", message: error)
    }
    
    func uploadSucceded() {
        showAlert(title: "Success", message: "\"mxkid.jpg\" file uploaded to OneDrive")
    }
    
}

extension ViewController:DropboxBrowserDelegate {
    func downloaded(data: Data, name: String) {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(name)
        do {
            try data.write(to: url)
        }
        catch {
            print(error)
        }
    }
}
