//
//  FTPViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 1/27/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework
import ActionSheetPicker_3_0

class FTPViewController: UIViewController, UITextFieldDelegate {

    let kDemoFileName = "mxkid.jpg"
    
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var viewSuccess: UIView!
    
    @IBOutlet weak var tfServer: UITextField!
    @IBOutlet weak var tfPort: UITextField!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPath: UITextField!
    
    var ftp = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnUpload.layer.borderColor = UIColor.lightGray.cgColor
        btnUpload.layer.borderWidth = 0.3
        btnUpload.layer.cornerRadius = 6
        btnUpload.clipsToBounds = true
        
        btnDone.layer.borderColor = UIColor.lightGray.cgColor
        btnDone.layer.borderWidth = 0.3
        btnDone.layer.cornerRadius = 6
        btnDone.clipsToBounds = true
        
        self.viewSuccess.isHidden = true
        // Do any additional setup after loading the view.
        title = ftp ? "FTP" : "SFTP"
    }
    
    //MARK: actions
    
    @IBAction func onUpload(_ sender: Any) {
        if tfUsername.text!.isEmpty {
            self.showAlert(title: "Error", message: "Username field is empty")
            return
        }
        if tfServer.text!.isEmpty {
            self.showAlert(title: "Error", message: "Host field is empty")
            return
        }
        if tfPassword.text!.isEmpty {
            self.showAlert(title: "Error", message: "Password field is empty")
            return
        }
        
        if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                if ftp {
                    let ftp = FTPController(server: tfServer.text!, port:tfPort.text!.isEmpty ? "21" : tfPort.text!, userName: tfUsername.text!, password: tfPassword.text!, directoryPath: tfPath.text!)
                    ftp.send(data: data, with: kDemoFileName) { (success) in
                        if success {
                            self.viewSuccess.isHidden = false
                        }
                        else {
                            self.showAlert(title: "Error", message: "Failed to upload file")
                        }
                    }
                }
                else {
                    let port:String = tfPort.text!.isEmpty ? "21" : tfPort.text!
                    if SFTPController.sharedInstance.uploadFile(host: tfServer.text!, port:Int(port)!, username: tfUsername.text!, password: tfPassword.text!, data: data, toRemotePath: "\(tfPath.text!)/\(kDemoFileName)") {
                        self.viewSuccess.isHidden = false
                    }
                    else {
                        self.showAlert(title: "Error", message: "Failed to upload file")
                    }
                }
                
            } catch {
                self.showAlert(title: "Error", message: "Failed to read file \(kDemoFileName)")
            }

        } else {
            showAlert(title: "Error", message: "Failed to read file \(kDemoFileName)")
        }
    }
    
    @IBAction func onDone(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    //MARK: textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
