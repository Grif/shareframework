//
//  WifiViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/5/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework

class WifiViewController: UIViewController, WifiDelegate {
    
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var btnScan: UIButton!
    
    @IBOutlet weak var txtConsole: UITextView!
    
    @IBOutlet weak var imgReceived: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgReceived.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(WifiViewController.tapOnImage))
        imgReceived.addGestureRecognizer(gesture)
        WifiController.sharedInstance.delegate = self
        
        btnSend.layer.cornerRadius = 10
        btnScan.layer.cornerRadius = 10
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if btnScan.isSelected {
            WifiController.sharedInstance.stop()
        }
    }
    
    @IBAction func onSend(_ sender: Any) {
        if !btnSend.isSelected {
            if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
                WifiController.sharedInstance.send(path:path,secretCode:"123123")
            }
            btnSend.setTitle("Stop", for: .normal)
            btnSend.isSelected = true
            btnScan.isEnabled = false
        }
        else {
            btnSend.setTitle("Send", for: .normal)
            btnSend.isSelected = false
            btnScan.isEnabled = true
            logMessage("stopped")
        }
    }
    
    @IBAction func onScan(_ sender: Any) {
        if !btnScan.isSelected {
            WifiController.sharedInstance.scan(secretCode: "123123")
            btnScan.setTitle("Stop", for: .normal)
            btnSend.isEnabled = false
            btnScan.isSelected = true
        }
        else {
            WifiController.sharedInstance.stop()
            btnScan.setTitle("Scan", for: .normal)
            btnSend.isEnabled = true
            btnScan.isSelected = false
        }
    }
    
    func logMessage(_ message: String) {
        txtConsole.text = txtConsole.text + "\n" + message
        
        let textCount: Int = txtConsole.text!.count
        guard textCount >= 1 else { return }
        txtConsole.scrollRangeToVisible(NSMakeRange(textCount - 1, 1))
    }
    
    func receivedFile(_ path: String) {
        if let image = UIImage(contentsOfFile: path) {
            imgReceived.isHidden = false
            imgReceived.image = image
        }
    }
    
    @objc func tapOnImage() {
        imgReceived.isHidden = true
    }
}
