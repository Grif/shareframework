//
//  WifiUploadViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/13/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework

class WifiUploadViewController: UIViewController {

    @IBOutlet weak var tfURL: UITextField!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    
    @IBOutlet weak var viewDownloadSuccess: UIView!
    @IBOutlet weak var imgDownloaded: UIImageView!
    @IBOutlet weak var btnDone: UIButton!
    
    
    @IBOutlet weak var tfDownloadURL: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tfURL.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 19, height: 20))
        tfURL.leftViewMode = .always
        tfURL.layer.cornerRadius = 10
        tfURL.layer.borderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1).cgColor
        tfURL.layer.borderWidth = 1
        
        tfURL.text = "http://localhost:8989/upload"
        
        tfDownloadURL.text = "http://image-mx.co.uk/wp-content/uploads/2012/02/ImageMX-Photography136.jpg"
        
        tfDownloadURL.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 19, height: 20))
        tfDownloadURL.leftViewMode = .always
        tfDownloadURL.layer.cornerRadius = 10
        tfDownloadURL.layer.borderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1).cgColor
        tfDownloadURL.layer.borderWidth = 1
        
        btnUpload.layer.cornerRadius = 10
        btnDownload.layer.cornerRadius = 10
        viewDownloadSuccess.isHidden = true
    }
    
    @IBAction func onUpload(_ sender: Any) {

        let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg")!
        
        HTTPController.sharedInstance.upload(tfURL.text!, filePath:path, fieldName: "mxkid.jpg") { (success, data) in
            if success {
                self.showAlert(title: "Success", message: "File uploaded")
            }
            else {
                self.showAlert(title: "Failed", message: data as! String)
            }
        }
        
//        Alamofire.upload(multipartFormData: { (data) in
//            data.append(URL(fileURLWithPath: path), withName: "file")
//        }, to: "http://localhost:8989/upload", method:.post) { (result) in
//
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.responseJSON { response in
//                    print("UPLOAD RESPONSE:http://localhost:8989/upload \(Date())", "BODY:\(NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)")
//                }
//
//            case .failure(let encodingError):
//                break
//            }
//        }
//        let postData = try! Data(contentsOf: URL(fileURLWithPath: path))

//
//        let headers = [
//            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
//            "cache-control": "no-cache",
//            "Postman-Token": "fbc9895c-70f1-465f-8d2a-426b3a49da5c"
//        ]
//        let parameters = [
//            [
//                "name": "file",
//                "fileName": "sound.aiff"
//            ],
//            [
//                "name": "filename",
//                "value": "sound.aiff"
//            ]
//        ]
//
//        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
//
//        var body = ""
//        var error: NSError? = nil
//        for param in parameters {
//            let paramName = param["name"]!
//            body += "--\(boundary)\r\n"
//            body += "Content-Disposition:form-data; name=\"\(paramName)\""
//            if let filename = param["fileName"] {
//                let contentType = "application/octet-stream"
//                do {
//                    var fileContent = try String(contentsOfFile: path, encoding: String.Encoding.ascii)
//
//                    if (error != nil) {
//                        print(error)
//                    }
//                    body += "; filename=\"\(filename)\"\r\n"
//                    body += "Content-Type: \(contentType)\r\n\r\n"
//                    body += fileContent
//                }
//                catch {
//
//                }
//            } else if let paramValue = param["value"] {
//                body += "\r\n\r\n\(paramValue)"
//            }
//        }
//
//        let request = NSMutableURLRequest(url: NSURL(string: "http://localhost:8989/upload")! as URL,
//                                          cachePolicy: .useProtocolCachePolicy,
//                                          timeoutInterval: 10.0)
//
//
//
//
//        request.httpMethod = "POST"
//        request.allHTTPHeaderFields = headers
//        request.httpBody = body.data(using: .utf8)
//
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//            if (error != nil) {
//                print(error)
//            } else {
//                let httpResponse = response as? HTTPURLResponse
//                print(httpResponse)
//            }
//        })
//
//        dataTask.resume()
//        return
//
//
        // the image in UIImage type
//        //let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg")!
//        let dataToUpload = try! Data(contentsOf: URL(fileURLWithPath: path))
//
//        let filename = "mxkid.jpg"
//
//        // generate boundary string using a unique per-app string
//        let boundary = UUID().uuidString
//
////        let fieldName = "reqtype"
////        let fieldValue = "fileupload"
////
////        let fieldName2 = "userhash"
////        let fieldValue2 = "caa3dce4fcb36cfdf9258ad9c"
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//        // Set the URLRequest to POST and to the specified URL
//        var urlRequest = URLRequest(url: URL(string: "http://localhost:8989/upload")!)
//        urlRequest.httpMethod = "POST"
//
//        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
//        // And the boundary is also set here
//        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        var data = Data()
//
//        // Add the reqtype field and its value to the raw http request data
////        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
////        data.append("Content-Disposition: form-data; filename=\"\(filename)\"\r\n\r\n".data(using: .utf8)!)
////        data.append("\(filename)".data(using: .utf8)!)
//
////        // Add the userhash field and its value to the raw http reqyest data
////        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
////        data.append("Content-Disposition: form-data; name=\"\(fieldName2)\"\r\n\r\n".data(using: .utf8)!)
////        data.append("\(fieldValue2)".data(using: .utf8)!)
////
//        // Add the image data to the raw http request data
//        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(path)\"\r\n".data(using: .utf8)!)
//        data.append(dataToUpload)
//
//        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
//        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
//        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
//
//        // Send a POST request to the URL, with the data we created earlier
//        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
//
//            if(error != nil){
//                print("\(error!.localizedDescription)")
//            }
//
//            guard let responseData = responseData else {
//                print("no response data")
//                return
//            }
//
//            if let responseString = String(data: responseData, encoding: .utf8) {
//                print("uploaded to: \(responseString)")
//            }
//        }).resume()
    }
    
    @IBAction func onDownload(_ sender: Any) {
        //http://image-mx.co.uk/wp-content/uploads/2012/02/ImageMX-Photography136.jpg
        let fileName = ProcessInfo.processInfo.globallyUniqueString
        let documentDirectoryPath: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileURL = URL(fileURLWithPath: documentDirectoryPath).appendingPathComponent(fileName)
        let filePath = fileURL.path
        
        HTTPController.sharedInstance.downloadFile(tfDownloadURL.text!, atPath: filePath) { (success, data) in
            if success {
                if let image = UIImage(contentsOfFile: filePath) {
                    self.imgDownloaded.image = image
                    self.viewDownloadSuccess.isHidden = false
                }
                else {
                    self.showAlert(title: "Success", message: "File downloaded.")
                }
            }
            else {
                self.showAlert(title: "Failed", message: data)
            }
        }
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    @IBAction func onDone(_ sender: Any) {
        viewDownloadSuccess.isHidden = true
        imgDownloaded.image = nil
    }
}
