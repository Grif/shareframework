//
//  BluetoothViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/5/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework

class BluetoothViewController: UIViewController, BluetoothDelegate {
    
    @IBOutlet weak var btnAdvertise: UIButton!
    @IBOutlet weak var btnScan: UIButton!
    
    @IBOutlet weak var txtConsole: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        BluetoothController.sharedInstance.logDelegate = self
        
        btnAdvertise.layer.cornerRadius = 10
        btnScan.layer.cornerRadius = 10
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if btnAdvertise.isSelected {
            BluetoothController.sharedInstance.stopAdvertising()
        }
        if btnScan.isSelected {
            BluetoothController.sharedInstance.stopScanning()
        }
    }
    
    @IBAction func onAdvetise(_ sender: Any) {
        if !btnAdvertise.isSelected {
            txtConsole.text = "console..."
            if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path))
                    BluetoothController.sharedInstance.startAdvertising(data: data)
                } catch {
                    self.showAlert(title: "Error", message: "Failed to read file")
                }
            }
            btnAdvertise.isSelected = true
            btnAdvertise.setTitle("Stop", for: .normal)
            btnScan.isEnabled = false
        }
        else {
            btnAdvertise.isSelected = false
            btnScan.isEnabled = true
            BluetoothController.sharedInstance.stopAdvertising()
            btnAdvertise.setTitle("Advertise (send)", for: .normal)
        }
        
    }
    
    @IBAction func onScan(_ sender: Any) {
        if !btnScan.isSelected {
            txtConsole.text = "console..."
            BluetoothController.sharedInstance.scanPeripherals()
            btnScan.isSelected = true
            btnScan.setTitle("Stop", for: .normal)
            btnAdvertise.isEnabled = false
        }
        else {
            btnAdvertise.isEnabled = true
            btnScan.setTitle("Scan (receive)", for: .normal)
            btnScan.isSelected = false
            BluetoothController.sharedInstance.stopScanning()
        }
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func logMessage(_ message: String) {
        txtConsole.text = txtConsole.text + "\n" + message
        
        let textCount: Int = txtConsole.text!.count
        guard textCount >= 1 else { return }
        txtConsole.scrollRangeToVisible(NSMakeRange(textCount - 1, 1))
    }
}
