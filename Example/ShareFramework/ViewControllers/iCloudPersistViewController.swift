//
//  iCloudPersistViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/9/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework

class iCloudPersistViewController: UIViewController {

    @IBOutlet weak var btnStore: UIButton!
    
    @IBOutlet weak var txtConsole: UITextView!
    var demoDict:[String:Any] = ["demoField1":"value for field 1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnStore.layer.cornerRadius = 6
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadData()
    }

    @IBAction func onStore(_ sender: Any) {
        if let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg") {
            demoDict["asset"] = URL(fileURLWithPath: path)
            
            iCloudController.sharedInstance.store(dict: demoDict, root:"DemoInfo") { (success, data) in
                if success {
                    self.logMessage("Success: data stored to iCloud")
                    self.loadData()
                }
                else {
                    self.logMessage("Failed \(data)")
                }
            }
        }
    }
    
    func loadData() {
        iCloudController.sharedInstance.fetchResults(root: "DemoInfo") { (success, data) in
            if success {
                self.logMessage((data as AnyObject).description)
            }
            else {
                self.logMessage("Found data cached for field DemoInfo")
                self.logMessage(data as! String)
            }
        }
    }
    
    func logMessage(_ message: String) {
        DispatchQueue.main.async {
            self.txtConsole.text = self.txtConsole.text + "\n" + message
            
            let textCount: Int = self.txtConsole.text!.count
            guard textCount >= 1 else { return }
            self.txtConsole.scrollRangeToVisible(NSMakeRange(textCount - 1, 1))
        }
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
}
