//
//  iCloudDriveViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/14/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework

class iCloudDriveViewController: UIViewController, iCloudDriveControllerDelegate {

    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    
    @IBOutlet weak var viewDownloadSuccess: UIView!
    @IBOutlet weak var imgDownloaded: UIImageView!
    @IBOutlet weak var btnDone: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnUpload.layer.cornerRadius = 10
        btnDownload.layer.cornerRadius = 10
        viewDownloadSuccess.isHidden = true
        
        iCloudDriveController.sharedInstance.delegate = self
    }
    
    @IBAction func onUpload(_ sender: Any) {
        let path = Bundle.main.path(forResource: "mxkid", ofType: "jpg")!
        iCloudDriveController.sharedInstance.uploadFile(path: path, viewController: self)
    }
    
    func uploadSucceded(url:URL) {
        if let data = try? Data(contentsOf: url) {
            if let image = UIImage(data: data) {
                self.imgDownloaded.image = image
                self.viewDownloadSuccess.isHidden = false
            }
        }
    }

    @IBAction func onDownload(_ sender: Any) {
        iCloudDriveController.sharedInstance.downloadFile(viewController: self)
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    @IBAction func onDone(_ sender: Any) {
        viewDownloadSuccess.isHidden = true
        imgDownloaded.image = nil
    }
}
