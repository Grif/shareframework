#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "NetworkManager.h"
#import "QNetworkAdditions.h"
#import "ReceiveServerController.h"
#import "SendController.h"
#import "ShareFramework.h"

FOUNDATION_EXPORT double ShareFrameworkVersionNumber;
FOUNDATION_EXPORT const unsigned char ShareFrameworkVersionString[];

