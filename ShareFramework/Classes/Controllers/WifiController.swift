//
//  WifiController.swift
//  ShareFramework
//
//  Created by Marius Avram on 2/1/19.
//

import UIKit

public protocol WifiDelegate{
    func logMessage(_ message:String)
    func receivedFile(_ path:String)
}


open class WifiController: NSObject, SendControllerDelegate, ReceiveServerControllerDelegate {
    @objc public static let sharedInstance = WifiController()
    open var delegate:WifiDelegate?
    
    override init() {
        super.init()
        SendController.sharedInstance().delegate = self
        ReceiveServerController.sharedInstance()?.delegate = self
    }
    
    open func send(path:String, secretCode:String) {
        SendController.sharedInstance().sendAction(path, secretCode:secretCode)
        if delegate != nil {
            delegate?.logMessage("sending file...")
        }
    }
    
    open func scan(secretCode:String) {
        ReceiveServerController.sharedInstance().transferServiceUUID = secretCode
        ReceiveServerController.sharedInstance().startServer(ReceiveServerController.sharedInstance().transferServiceUUID)
        if delegate != nil {
            delegate?.logMessage("scanning...")
        }
    }
    
    open func stop() {
        ReceiveServerController.sharedInstance()?.stopServer(nil)
        if delegate != nil {
            delegate?.logMessage("stopped")
        }
    }
    
    //send delegate
    public func transferCompleted() {
        if delegate != nil {
            delegate?.logMessage("transfer completed")
        }
    }
    
    public func transferFailed() {
        if delegate != nil {
            delegate?.logMessage("transfer failed")
        }
    }
    
    
    //receive delegate
    public func transferCompleted(_ file: String!) {
        if delegate != nil {
            delegate?.receivedFile(file)
            delegate?.logMessage("file received")
        }
    }
}
