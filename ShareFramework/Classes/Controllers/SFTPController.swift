//
//  SFTPController.swift
//  Pods
//
//  Created by Marius Avram on 1/28/19.
//

import UIKit
import NMSSH

public class SFTPController: NSObject {
    @objc public static let sharedInstance = SFTPController()

    public func uploadFile(host: String, port:Int, username: String, password:String, data:Data, toRemotePath:String) -> Bool {
        let session = NMSSHSession(host: host, port: port, andUsername: username)
        session.connect()
        
        if session.isConnected {
            session.authenticate(byPassword: password)
            if session.isAuthorized {
                print("authorized")
            }
            else {
                return false
            }
        }
        else {
            return false
        }
        
        let sftp = NMSFTP.connect(with: session)
        
        if !sftp.writeContents(data, toFileAtPath: toRemotePath) {
            sftp.disconnect()
            
            session.disconnect()
            
            return false
        }
        
        sftp.disconnect()
        
        session.disconnect()
        
        return true
    }
}
