//
//  GoogleDriveViewController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/19/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import GTMAppAuth

public protocol GoogleDriveViewControllerDelegate {
    func uploadFailed(error:String)
    func uploadSucceded()
}


open class GoogleDriveViewController: NSObject {
    @objc public static let sharedInstance = GoogleDriveViewController()
    
    let kIssuer = "https://accounts.google.com"
    var kClientID = "387536636889-oei66qgtpnkpcdehafhv81sthoelk8gm.apps.googleusercontent.com"
    var kRedirectURI = "com.googleusercontent.apps.387536636889-oei66qgtpnkpcdehafhv81sthoelk8gm:/oauthredirect"
    
    
    var authFlow:OIDAuthorizationFlowSession?
    var state:OIDAuthState!
    open var delegate:GoogleDriveViewControllerDelegate!
    
    open func initialize(clientId:String,redirectURI:String) {
        kClientID = clientId
        kRedirectURI = redirectURI
    }
    
    open func upload(viewController:UIViewController, fileName:String, filePath:String) {
        let issuer = URL(string: kIssuer)
        let redirectURI = URL(string: kRedirectURI)
        
        OIDAuthorizationService.discoverConfiguration(forIssuer: issuer!) { (config, error) in
            if config == nil {
                print("Error retrieving discovery document \(error!.localizedDescription)")
                if self.delegate != nil {
                    self.delegate.uploadFailed(error: "Error retrieving discovery document \(error!.localizedDescription)")
                }
                return
            }
            
            print("Got configuration \(config!)")
            
            let request = OIDAuthorizationRequest(configuration: config!, clientId: self.kClientID, scopes: [OIDScopeOpenID, OIDScopeProfile, "https://www.googleapis.com/auth/drive"], redirectURL: redirectURI!, responseType: OIDResponseTypeCode, additionalParameters: nil)
            self.authFlow = OIDAuthState.authState(byPresenting: request, presenting: viewController, callback: { (state, error) in
                if error != nil {
                    return
                }
                self.state = state
                if state != nil {
                    let authorization = GTMAppAuthFetcherAuthorization(authState: state!)
                    let serviceDrive = GTLRDriveService()
                    serviceDrive.authorizer = authorization
                    serviceDrive.isRetryEnabled = true
                    
                    let file = GTLRDrive_File()
                    file.name = fileName
                    file.descriptionProperty = "Uploaded from ShareFramework"
                    //file.mimeType = "audio/aiff"
            
                    let data = FileManager.default.contents(atPath: filePath)
                    let uploadParams = GTLRUploadParameters(data: data!, mimeType: "")
                    let query = GTLRDriveQuery_FilesCreate.query(withObject: file, uploadParameters: uploadParams)
            
                    serviceDrive.executeQuery(query, completionHandler: { (ticket, insertedFile, error) in
                        if error == nil {
                            if self.delegate != nil {
                                self.delegate.uploadSucceded()
                            }
                        }
                        else {
                            if self.delegate != nil {
                                self.delegate.uploadFailed(error: "Error retrieving discovery document \(error!.localizedDescription)")
                            }
                        }
                    })
                }
                else {
                    print("Authorization error \(error!.localizedDescription)")
                }
            })
        }
    }
    
    open func list(viewController:UIViewController) {
        let issuer = URL(string: kIssuer)
        let redirectURI = URL(string: kRedirectURI)
        
        OIDAuthorizationService.discoverConfiguration(forIssuer: issuer!) { (config, error) in
            if config == nil {
                print("Error retrieving discovery document \(error!.localizedDescription)")
                if self.delegate != nil {
                    self.delegate.uploadFailed(error: "Error retrieving discovery document \(error!.localizedDescription)")
                }
                return
            }
            
            print("Got configuration \(config!)")
            
            let request = OIDAuthorizationRequest(configuration: config!, clientId: self.kClientID, scopes: [OIDScopeOpenID, OIDScopeProfile, "https://www.googleapis.com/auth/drive"], redirectURL: redirectURI!, responseType: OIDResponseTypeCode, additionalParameters: nil)
            self.authFlow = OIDAuthState.authState(byPresenting: request, presenting: viewController, callback: { (state, error) in
                if error != nil {
                    return
                }
                let authorization = GTMAppAuthFetcherAuthorization(authState: state!)
                let serviceDrive = GTLRDriveService()
                serviceDrive.authorizer = authorization
                serviceDrive.isRetryEnabled = true
            
            
                let q = GTLRDriveQuery_FilesList.query()

                serviceDrive.executeQuery(q) { (ticket, data, error) in
                    if error == nil {
                        
                    }
                }
            })
        }
//        let uploadParams = GTLRUploadParameters(data: data!, mimeType: "")
//        let query = GTLRDriveQuery_FilesCreate.query(withObject: file, uploadParameters: uploadParams)
//
//        serviceDrive.executeQuery(query, completionHandler: { (ticket, insertedFile, error) in
//            if error == nil {
//                if self.delegate != nil {
//                    self.delegate.uploadSucceded()
//                }
//            }
//            else {
//                if self.delegate != nil {
//                    self.delegate.uploadFailed(error: "Error retrieving discovery document \(error!.localizedDescription)")
//                }
//            }
//        })
    }
    
}
