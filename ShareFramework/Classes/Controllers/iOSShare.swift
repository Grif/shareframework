//
//  iOSShare.swift
//  ShareFramework
//
//  Created by Marius Avram on 1/30/19.
//

import UIKit

open class iOSShare: NSObject {
    @objc public static let sharedInstance = iOSShare()

    open func airDropShare(fromViewController:UIViewController, file:String) {
        let controller = UIActivityViewController(activityItems: [URL(fileURLWithPath: file)], applicationActivities: nil)
        controller.excludedActivityTypes = [.postToWeibo, .message, .mail, .print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .addToReadingList, .postToFlickr, .postToVimeo, .postToTencentWeibo, .postToFacebook, .postToTwitter]
        
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        
        fromViewController.present(controller, animated: true, completion: nil)
    }
}
