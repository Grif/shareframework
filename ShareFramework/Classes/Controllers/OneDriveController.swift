//
//  OneDriveController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/18/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ShareFramework
import OneDriveSDK

public protocol OneDriveControllerDelegate {
    func uploadFailed(error:String)
    func uploadSucceded()
}

open class OneDriveController: NSObject {

    @objc public static let sharedInstance = OneDriveController()
    open var delegate:OneDriveControllerDelegate!
    
    open func upload(accountAppId:String, filePath:String, fileName:String) {
        let scopes = ["wl.signin", "wl.basic", "wl.skydrive", "wl.skydrive_update"] as [Any]
        
        ODClient.setMicrosoftAccountAppId(accountAppId, scopes: scopes)
        
        ODClient.client { (client, error) in
            if error == nil {
                let req = client?.root()?.item(byPath: fileName)?.contentRequest()
                req?.upload(fromFile: URL(fileURLWithPath: filePath), completion: { (item, error) in
                    if error == nil {
                        if self.delegate != nil {
                            self.delegate.uploadSucceded()
                        }
                    }
                    else if self.delegate != nil {
                        self.delegate.uploadFailed(error: error.debugDescription)
                    }
                })
            }
            else if self.delegate != nil {
                self.delegate.uploadFailed(error: error.debugDescription)
            }
        }
    }
}
