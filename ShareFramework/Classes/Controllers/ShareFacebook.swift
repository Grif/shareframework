//
//  ShareFacebook.swift
//  FBSnapshotTestCase
//
//  Created by Marius Avram on 1/11/19.
//

import Foundation
//import FacebookShare
import FBSDKShareKit

public class ShareFacebook : NSObject {
    @objc public static let sharedInstance = ShareFacebook()

    public func shareURL(_ url:URL, from:UIViewController) {
        let content = ShareLinkContent()
        content.contentURL = url
        let shareDialog = ShareDialog(fromViewController: from, content: content, delegate: nil)
        shareDialog.show()
        //shareDialog.mode = .native
//        shareDialog.failsOnInvalidData = true
//        shareDialog.completion = { result in
//            // Handle share results
//        }
//        do {
//            try shareDialog.show()
//        } catch {
//
//        }
    }
    
    public func videoShare(_ url:URL, from:UIViewController) {
        let content = ShareVideoContent()
        content.video = ShareVideo(videoURL: url)
        let shareDialog = ShareDialog(fromViewController: from, content: content, delegate: nil)
        shareDialog.show()
//        let shareDialog = ShareDialog(content: content)
//        //shareDialog.mode = .native
//        shareDialog.failsOnInvalidData = true
//        shareDialog.completion = { result in
//            // Handle share results
//        }
//        do {
//            try shareDialog.show()
//        } catch {
//
//        }
    }
    
    public func imageShare(_ url:URL, from:UIViewController) {
        let content = SharePhotoContent()
        content.photos = [SharePhoto(imageURL: url, userGenerated: true)]
        let shareDialog = ShareDialog(fromViewController: from, content: content, delegate: nil)
        shareDialog.show()
    }
    
    public func imageShare(_ image:UIImage, from:UIViewController) {
        let content = SharePhotoContent()
        content.photos = [SharePhoto(image: image, userGenerated: true)]
        let shareDialog = ShareDialog(fromViewController: from, content: content, delegate: nil)
        shareDialog.mode = .automatic
        shareDialog.show()
    }
}
