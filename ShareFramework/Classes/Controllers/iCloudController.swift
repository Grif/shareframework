//
//  iCloudController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/8/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import CloudKit

open class iCloudController: NSObject {
    @objc public static let sharedInstance = iCloudController()
    
    
    open func store(dict:[String:Any], root:String, completionHandler: @escaping (Bool, String) -> Void) {
        let container = CKContainer.default()
        let publicDB = container.privateCloudDatabase
        let userData = CKRecord(recordType: root)
        
        for item in dict {
            if item.value is URL {
                let asset = CKAsset(fileURL: item.value as! URL)
                userData.setObject(asset, forKey: item.key)
            }
            else {
                userData.setValue(item.value, forKey: item.key)
            }
        }
        DispatchQueue.main.async {
            publicDB.save(userData, completionHandler: { (record, error) -> Void in
                // print("saved")
                if error != nil{
                    completionHandler(false,error!.localizedDescription)
                }
                else {
                    completionHandler(true,"")
                }
            })
        }
    }
    
    open func fetchResults(root:String, completionHandler: @escaping (Bool, Any) -> Void) {
        
        let container = CKContainer.default()
        let privateDatabase = container.privateCloudDatabase
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: root, predicate: predicate)
        
        privateDatabase.perform(query, inZoneWith: nil) { (results, error) -> Void in
            if error != nil {
                completionHandler(false,error!.localizedDescription)
                
            }
            else {
                var data = [String:Any]()
                for result in results! {
                    for key in result.allKeys() {
                        if let value = result.value(forKey: key) {
                            if value is CKAsset {
                               data[key] = (value as! CKAsset).fileURL
                            }
                            else {
                                data[key] = value
                            }
                        }
                    }
                }
                completionHandler(true,data)
            }
        }
    }
}
