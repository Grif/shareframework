//
//  iCloudDriveController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/14/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

public protocol iCloudDriveControllerDelegate {
    func uploadSucceded(url:URL)
}

open class iCloudDriveController: NSObject, UIDocumentPickerDelegate {
    @objc public static let sharedInstance = iCloudDriveController()
    
    open var delegate:iCloudDriveControllerDelegate!
    
    open func uploadFile(path:String, viewController:UIViewController) {
        let vc = UIDocumentPickerViewController(url: URL(fileURLWithPath: path), in: UIDocumentPickerMode.exportToService)
        vc.delegate = self
        viewController.present(vc, animated: true) { () -> Void in
        }
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        _ = url.startAccessingSecurityScopedResource()

        FileManager.default.url(forUbiquityContainerIdentifier: nil)

        let fileCoordinator = NSFileCoordinator()
        fileCoordinator.coordinate(readingItemAt: url, options: NSFileCoordinator.ReadingOptions(), error: nil) { (newURL :URL!) -> Void in
            if self.delegate != nil {
                self.delegate.uploadSucceded(url:newURL!)
            }
        }
        url.stopAccessingSecurityScopedResource()
    }
    
    open func downloadFile(viewController:UIViewController) {
        let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.data"], in: UIDocumentPickerMode.open)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        viewController.present(documentPicker, animated: true, completion: nil)
    }
}
