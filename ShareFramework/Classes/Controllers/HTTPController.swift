//
//  HTTPController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/14/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import Alamofire

open class HTTPController: NSObject {
    @objc public static let sharedInstance = HTTPController()
    
    open func upload(_ toUrl:String, filePath:String, fieldName:String, parameters:[String:Any]? = nil, completionHandler:((Bool, Any) -> Void)?) {
        Alamofire.upload(multipartFormData: { (data) in
            data.append(URL(fileURLWithPath: filePath), withName: fieldName)
            if parameters != nil {
                for (key, value) in parameters! {
                    data.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
            
        }, to: toUrl, method:.post) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print("UPLOAD RESPONSE:\(toUrl) \(Date())", "BODY:\(NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)")
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String:Any]
                        if completionHandler != nil {
                            completionHandler!(true, json)
                        }
                        return
                    } catch {
                        print("Upload response not json formatted.")
                    }
                    if completionHandler != nil {
                        completionHandler!(response.error != nil ? false : true, response.error != nil ? response.error!.localizedDescription : "")
                    }
                }
                
            case .failure(let encodingError):
                print("UPLOAD RESPONSE:\(toUrl) \(Date())", "ERROR:\(encodingError.localizedDescription)")
                if completionHandler != nil {
                    completionHandler!(false, encodingError.localizedDescription)
                }
                break
            }
        }
    }
    
    open func downloadFile(_ fromURL:String, atPath:String, completionHandler:((Bool, String) -> Void)?) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let url = URL(fileURLWithPath: atPath)
            return (url, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(fromURL, to: destination).response { response in
            print(response)
            if response.error == nil {
                if completionHandler != nil {
                    completionHandler!(true, "")
                }
            }
            else {
                if completionHandler != nil {
                    completionHandler!(false, response.error.debugDescription)
                }
            }
        }
    }
    
}
