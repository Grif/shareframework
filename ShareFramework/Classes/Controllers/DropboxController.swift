//
//  DropboxController.swift
//  ShareFramework_Example
//
//  Created by Marius Avram on 2/18/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import SwiftyDropbox

open class DropboxController: NSObject {
    @objc public static let sharedInstance = DropboxController()
    
    open func authenticateToDropbox(controller:UIViewController) {
        DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                      controller: controller,
                                                      openURL: { (url: URL) -> Void in
                                                        UIApplication.shared.openURL(url)
        })
    }
    
    open func upload(folder:String, fileName:String, fileData:Data, completionHandler:((Bool, String) -> Void)?) {
        let client = DropboxClientsManager.authorizedClient
        client?.files.upload(path: "/\(folder)/\(fileName)", input: fileData)
            .response { response, error in
                if let response = response {
                    if completionHandler != nil {
                        completionHandler!(true,"")
                    }
                    print(response)
                } else if let error = error {
                    print(error)
                    if completionHandler != nil {
                        completionHandler!(false,"\(error)")
                    }
                }
            }
            .progress { progressData in
                print(progressData)
        }
    }
    
    open func showDownload(parentController:UIViewController) -> DropboxBrowserViewController {
        let overlayVC = DropboxBrowserViewController(nibName: "DropboxBrowserViewController", bundle: Bundle(for: DropboxBrowserViewController.self))
        parentController.show(overlayVC, sender: self)//navigationController?.pushViewController(overlayVC, animated: true)
        
        return overlayVC
    }
    
    open func listFolders(folder:String, completionHandler:((Bool, [String:Bool]) -> Void)?) {
        DropboxClientsManager.authorizedClient?.files.listFolder(path: folder).response(completionHandler: { (folders, errors) in
            if errors == nil {
                var returnFolders = [String:Bool]()
                for folder in folders!.entries {
                    returnFolders[folder.name] = folder is Files.FolderMetadata
                }
                if completionHandler != nil {
                    completionHandler!(true, returnFolders)
                }
            }
            else {
                if completionHandler != nil {
                    completionHandler!(false, [String:Bool]())
                }
            }
        })
    }
    
    open func download(path:String, completionHandler:((Bool, String, Data?) -> Void)?) {
        DropboxClientsManager.authorizedClient?.files.download(path: path).response(completionHandler: { (response, error) in
            if let response = response {
                if completionHandler != nil {
                    completionHandler!(true,response.0.name, response.1)
                }
                print(response)
            } else if let error = error {
                print(error)
                if completionHandler != nil {
                    completionHandler!(false,"\(error)", nil)
                }
            }
        })
    }
}
