//
//  DropboxBrowserViewController.swift
//  ShareFramework
//
//  Created by Marius Avram on 8/26/19.
//

import UIKit

public protocol DropboxBrowserDelegate:NSObjectProtocol {
    func downloaded(data:Data, name:String)
}

public class DropboxBrowserViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var folders = [String:Bool]()
    var currentFolder = ""
    
    var loading = false
    
    public weak var delegate:DropboxBrowserDelegate!
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "DropboxFolderTableViewCell", bundle: Bundle(for: DropboxFolderTableViewCell.self)), forCellReuseIdentifier: "dropboxFolderTableViewCell")
        
        loading = true
        DropboxController.sharedInstance.listFolders(folder:currentFolder) { (success, folders) in
            self.folders = folders
            self.tableView.reloadData()
            self.loading = false
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController != nil {
            tableTopConstraint.constant = 0
        }
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension DropboxBrowserViewController: UITableViewDataSource, UITableViewDelegate {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folders.count + 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dropboxFolderTableViewCell", for: indexPath)
        cell.accessoryType = .disclosureIndicator
        
        if indexPath.row == 0 {
            if let lbl = cell.contentView.viewWithTag(1) as? UILabel {
                lbl.text = "..."
            }
        }
        else {
            let index = folders.keys.index(folders.keys.startIndex, offsetBy: indexPath.row - 1)
            if let lbl = cell.contentView.viewWithTag(1) as? UILabel {
                lbl.text = folders[index].key
            }
            if !folders[index].value {
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.setSelected(false, animated: true)
        }
        if indexPath.row == 0 {
            if !currentFolder.contains("/") {
                currentFolder = ""
            }
            else {
                let strIndex = currentFolder.lastIndex(of: Character("/"))
                let range = strIndex! ..< currentFolder.endIndex
                currentFolder.removeSubrange(range)
            }
            DropboxController.sharedInstance.listFolders(folder:currentFolder) { (success, folders) in
                self.folders = folders
                self.tableView.reloadData()
                self.loading = false
            }
        }
        else {
            let index = folders.keys.index(folders.keys.startIndex, offsetBy: indexPath.row  - 1)
            if !folders[index].value {
                let alert = UIAlertController(
                    title: "Download \(folders[index].key)?",
                    message: "",
                    preferredStyle: UIAlertController.Style.alert
                )
                let ok = UIAlertAction(title: "Yes", style: .default) { (action) in
                    DropboxController.sharedInstance.download(path: "\(self.currentFolder)/\(self.folders[index].key)", completionHandler: { (success, error, data) in
                        if success {
                            self.dismiss(animated: true) {
                                if self.delegate != nil {
                                   self.delegate!.downloaded(data: data!, name: error)
                                }
                            }
                        }
                    })
                }
                alert.addAction(ok)
                let cancel = UIAlertAction(title: "No", style: .cancel) { (action) in
                    
                }
                alert.addAction(cancel)
                present(alert, animated: true, completion: nil)
            }
            else {
                if loading {
                    return
                }
                loading = true
                
                if currentFolder.isEmpty {
                    currentFolder = "/\(folders[index].key)"
                }
                else {
                    currentFolder = "\(currentFolder)/\(folders[index].key)"
                }
                DropboxController.sharedInstance.listFolders(folder:currentFolder) { (success, folders) in
                    self.folders = folders
                    self.tableView.reloadData()
                    self.loading = false
                }
            }
        }
    }
}
